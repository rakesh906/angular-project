// import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
// import {AuthenticationService} from '../_services/authentication.service';
// import { Observable } from 'rxjs';
// // import { request } from 'http';
// export class JwrInterceptor implements HttpInterceptor
// {
//     constructor(private authenticationService:AuthenticationService){}
// intercept(request:HttpRequest<any>,next:HttpHandler):Observable<HttpEvent<any>>
// {
// let currentUser=this.authenticationService.currentUserValue;
// if(currentUser && currentUser.token)
// {
//     request=request.clone({
//         setHeaders:
//         {
//             Authorization:'Bearer ${currentUser.token}'
//         }
//     });
// }

// return next.handle(request);
// }

// }

import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';

import { AuthenticationService } from '../_services/authentication.service';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
    constructor(private authenticationService: AuthenticationService) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // add authorization header with jwt token if available
        let currentUser = this.authenticationService.currentUserValue;
        if (currentUser && currentUser.token) {
            request = request.clone({
                setHeaders: {
                    Authorization: `Bearer ${currentUser.token}`
                }
            });
        }

        return next.handle(request);
    }
}