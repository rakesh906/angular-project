import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './_home/home.component';
import { LoginComponent } from './_login/login/login.component';
import { AuthGuard } from './_helper/auth.guard';

const routes: Routes = [{path:'',component:HomeComponent,canActivate:[AuthGuard]},
{path:'login' ,component:LoginComponent},
{path:"**",redirectTo:''}
];
// const routes: Routes = [];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
