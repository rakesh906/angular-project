import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {HttpClientModule,HTTP_INTERCEPTORS} from '@angular/common/http';

import { HomeComponent } from './_home/home.component';
import { LoginComponent } from './_login/login/login.component';
import { fakeBackendProvider } from './_helper/fake-backend';
import { ErrorInterceptor } from './_helper/error.interceptor';
import { JwtInterceptor} from './_helper/jwt.interceptor';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,HomeComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,    ReactiveFormsModule,
    HttpClientModule,
  ],
  providers: [{provide:HTTP_INTERCEPTORS,useClass:JwtInterceptor,multi:true},{provide:HTTP_INTERCEPTORS,useClass:ErrorInterceptor,multi:true},fakeBackendProvider],
  bootstrap: [AppComponent]
})
export class AppModule { }
